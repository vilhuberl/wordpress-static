#!/bin/bash
#
# Clone Wordpress into Static HTML
#
# @ April 6th, 2012 - Gino O'Donnell - SeattleIT.net  
# @ April 16th, 2014 - Lars Vilhuber

### BEGIN USER VARIABLES -- EDIT THESE ###

# Your WordPress domain:
SITE="www2.vrdc.cornell.edu"
# Subdirectory - leave blank if your site is in the root
SITESUBDIR="/news"
# Path to your WordPress installation
WPROOTDIR="/var/www/wordpress/"; 
# Where to save to:
SAVETO="/ramdisk/www2.vrdc.cornell.edu/"
# Comma seperated of directories to exclude:
EXCLUDE='/static'
# Space seperated list of directories to copy:
OTHERSTUFFTOCOPY="/var/www/yourdomain/htdocs/wordpress/static/ \
                  /var/www/yourdomain/htdocs/wordpress/somepage.html \
                  /var/www/yourdomain/htdocs/wordpress/pic.gif \
                  /var/www/yourdomain/htdocs/wordpress/favicon.ico"
                  
### END USER VARIABLES ###

[[ -d $SAVETO ]] || mkdir -p $SAVETO
[[ -z $SITESUBDIR ]] || mkdir -p $SAVETO$SITESUBDIR
cd $SAVETO
# Grab the site recursively using wget (remove -q to un-quiet):
wget -q -X "$EXCLUDE" -U "Wordpress Clone Agent" \
                      -e robots=off \
                      --mirror -p --html-extension \
                      --base=.${SITESUBDIR}/ -P .${SITESUBDIR}/ http://$SITE$SITESUBDIR/
                      
cd $SAVETO

for PAGE in `find * -name "*.html"`; do 
  DIR=`echo $PAGE | sed "s/\.html//"`;
  if [[ ! "$PAGE" =~ ^index ]]; then 
    if [[ ! "$PAGE" =~ \.1.html$ ]]; then   
      echo "[+] Creating directory $DIR"
      mkdir -p $DIR;
      echo "[+] Setting up $PAGE $DIR/index.html"
      mv $PAGE $DIR/index.html; 
      echo "[+] Removing uneeeded page $PAGE"
     else 
      # Handle subindex pages
      DIR=`echo $DIR | sed "s/\.1//"`
      echo "[+]  Moving subindex page $PAGE to $DIR/index.html"
      mv "$PAGE" $DIR/index.html;
    fi
  fi
done

echo "[+] Moving primary index page"
mv $SITE/index/index.html $SITE/index.html
rm -rf $SITE/index/

# Copy extra directories here:
#for FILES in $OTHERSTUFFTOCOPY; do cp -a $FILES $SITE/; done
# 
#cd "$SITE"
#QSASSETS=$(for FILE in `for F in *.{js\?*,css\?*}; do find * -name "$F" ; done`; do ls $FILE | sed "s/\?.*//g"; done | sort | uniq); 
# Remove files with query strings
#for ASSET in ${QSASSETS}; do cp $WPROOTDIR/$ASSET $ASSET; done

# Remove the cruff
find * -name "index?p*" -exec rm -rf {} \;
for FILE in `for F in *.{js?*,css?*}; do find * -name "$F" ; done`; do rm $FILE ; done

